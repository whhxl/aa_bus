const formateTime= ()=>{
  const today = new Date()
  const years = today.getFullYear()
  const mouth = today.getMonth() + 1
  const day = today.getDate()
  today.setMonth(mouth - 2)
  today.setDate(0)
  const days = today.getDate()
  return { dates: years + '-' + mouth + '-' + days, todays: years + '年' + mouth + '月' + day + '日', checktime: years + '-' + (mouth < 10 ? '0' + mouth : mouth) + '-' + day}
}
const randomnum = ()=>{
  return Math.floor(Math.random()*10-1+1)
}
const createSign= () =>{
  const radom = randomnum()
  // console.log(radom)
  const md5 = require('md5.js');
  var str = md5.hex_md5('20LingNan@Small&Project19');
  // console.log(str);
  var newradom = Math.floor(Math.random() * 30);
  // console.log(newradom);
  var arr = [];
  var numbers = Math.ceil(32 / newradom);
  var split1 = 0;
  var split2 = newradom;
  for (var i = 0; i < numbers; i++) {
    arr.push(str.slice(split1, split2));
    split1 = split2;
    split2 = split2 + newradom;
  }
  var date = new Date();
  var now = Math.floor(date.getTime() / 1000);
  var newstr = now;
  for (var i = 0; i < arr.length; i++) {
    newstr += arr[i];
    if (i == 0) {
      newstr += newradom;
    }
  }
  var data = {
    'sign': md5.hex_md5(newstr),
    'h': newradom * 10 / 5 + radom,
    'now': now,
    'magnitude': radom
  }
  return data;
  // console.log(newstr);
}
module.exports = {
  formateTime: formateTime,
  createSign: createSign,
  randomnum: randomnum
}
