//index.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util')
Page({
  data: {
    showadres: 0,
    showcalendar: 0,
    teday: '',
    choseTime: '',
    placeTypes: '',
    startplace: '',
    endplace: '',
    tabitem:[
      '定制巴士',
      '旅游线路',
      '旅游产品'
    ],
    tabIndex: 0,
    imgurl: []
  },
 
  onLoad: function () {
    if(app.globalData.sesion_id==""){
      app.login()
    }
    let choseDate = wx.getStorageSync('choseDate'),
        startplace = wx.getStorageSync('startplace'),
        endplace = wx.getStorageSync('endplace')
      this.setData({
        isShowpop: app.globalData.isSetininfo,
        startplace:startplace,
        endplace:endplace,
        teday: util.formateTime().dates,
        choseTime: choseDate.choseDate || util.formateTime().todays
      })
    wx.setStorageSync('choseDate', {
      'choseday': choseDate.choseday || util.formateTime().checktime,
      'dates': util.formateTime().dates
      })
    setTimeout(() => {
      // console.log(app.globalData)
      this.setData({
        isShowpop: app.globalData.isSetininfo
      })
    }, 400)
    // app.login()
    this.loadBanner()
  },


  // 时间选择
  choseTime(e){
    wx.navigateTo({
      url: '/pages/calendar/calendar',
    })
  },

  // 选择地址
  choseadress(e){
    const palcetype = e.currentTarget.dataset.types
    if (palcetype =='starts'){
      wx.navigateTo({
        url: '/pages/adress/adress?status=start&type=',
      })
    } else if (palcetype == 'ends'){
      const start_city = wx.getStorageSync('startplace')
      if(!start_city){
        wx.showToast({
          title: '请选择出发地',
          icon: 'none'
        })
        return
      }
      wx.navigateTo({
        url: '/pages/adress/adress?status=ends&type=start_city',
      })
    }

  },
  // 关闭日历组件
  closecalendr(){
    this.setData({
      showcalendar: 0
    })
  },
  // 关闭地址组件
  closeadres(){
    this.setData({
      showadres: 0,
    })
  },
  selecttab(e){
    const currIndex = e.currentTarget.dataset.index
    this.setData({
      tabIndex: currIndex
    })
  },
  // 查询班次
  checkedcart(){
    let intl = '';
    const start_city = wx.getStorageSync('startplace') || '';
    const arve_city = wx.getStorageSync('endplace') || '';
    switch(true){
      case !start_city: intl = '请输入出发地'; break;
      case !arve_city: intl = '请输入目的地'; break; 
    }
    if(intl !=''){
      wx.showToast({
        title: intl,
        icon: 'none'
      })
      return
    }
    wx.navigateTo({
      url: '../busline/busline?choseday=' + this.data.choseTime,
    })
  },
  // 获取用户登录信息
  getUserInfo(e){
    app.globalData.userInfo = e.detail
    app.login()
    app.globalData.isSetininfo = true
    this.setData({
      isShowpop: false
    })
  },
  // 获取banner
  loadBanner(){
    if (this.data.imgurl.length) return
    const data = util.createSign()
    const params={...data}
    app.http(params, app.globalData.damain+'Banner/get_banner').then(res=>{
      if(res.data.banner.length>0){
        res.data.banner.forEach(item=>{
          item.img = res.data.domain + item.img
        })
        this.setData({
          imgurl: res.data.banner
        })
      }
    })
  },
  onUnload: function () {
      // wx.removeStorageSync('choseDate')
      // wx.removeStorageSync('startplace')
      // wx.removeStorageSync('endplace')
  },
})
