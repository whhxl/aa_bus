// pages/commphone/commphone.js
const app =getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phoneNum: '',
    codeNum: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  setphone(e){
    this.setData({
      phoneNum: e.detail.value
    })
  },
  setcode(e){
    this.setData({
      codeNum: e.detail.value
    })
  },
  //  获取手机验证吗
  getcheckCode(){
    const apiurl = app.globalData.damain + 'Users/send_msg'
    const phoneNum = this.data.phoneNum
    if (phoneNum==''){
      wx.showToast({
        title: '请输入手机号码',
        icon: 'none'
      })
      return
    }
    if(this.checkPhone(phoneNum)){
      app.http(
        { 
          tel: phoneNum, 
          session_id: app.globalData.session_id
          },apiurl
          ).then(res=>{
            wx.showToast({
              title: res.msg,
              icon: 'none'
            })
          })
    }
  },
  checkPhone(num) {
    if (!(/^1[34578]\d{9}$/.test(num))) {
      return false;
    }else{
      return true
    }
  },
  // 绑定手机号码
   bindphone(){
     const apiurl = app.globalData.damain + 'Users/bind_phone'
     if(this.data.phoneNum == ''){
       wx.showToast({
         title: '请输入手机号',
         icon: 'none'
       })
       return
     }
     app.http(
       {
         tel: this.data.phoneNum,
         session_id: app.globalData.session_id,
         code: this.data.codeNum
       },
       apiurl
     ).then(res=>{
       if(res.code == 1){
         wx.showToast({
           title: res.msg,
           icon: 'none'
         })
         setTimeout(()=>{
           wx.redirectTo({  
            url: '/pages/commphone/mytel',
          })
         },800)
       }else{
         wx.showToast({
           title: res.msg,
           icon: 'none'
         })
       }
     })
   },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})