// pages/common/adress.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
  adress: [],
  placestatus: '',
  loding: true,
  },
  onLoad: function(options) {
      this.setData({
        placestatus: options.status
      })
    if (options.status =='start'){
      wx.setNavigationBarTitle({
        title: '出发地'
      })
      this.getcityLisst(options.type)
    } else if (options.status == 'ends'){
      wx.setNavigationBarTitle({
        title: '目的地'
      })
      this.getcityLisst(options.type)
    }
    
  },
    choseadress(e){
      const adress = e.currentTarget.dataset.adress,
            palcetype = this.data.placestatus
      if (palcetype=='start'){
        wx.setStorageSync('startplace', adress)
        wx.removeStorageSync('endplace')
      }else{
        wx.setStorageSync('endplace', adress)
      }
      wx.reLaunch({
        url: '/pages/index/index',
      })
    },
    getcityLisst(types){
      const linkapi = types != '' ? '/City/arrive' : '/City/start'
      wx.request({
        url: app.globalData.damain + linkapi,
        header: { 'content-type': 'application/x-www-form-urlencoded' },
        method: 'POST',
        dataType: 'json',
        data: types != '' ? { 
        start_city: wx.getStorageSync('startplace')} : '',
        success: ({data})=>{
          if(data.code ==1){
            this.setData({
              adress: data.data,
              loding: false
            })
          }else{
            wx.showToast({
              title: '获取出发地失败',
              icon: 'none'
            })
          }
        }
        })
    }
    // closeadrescompent(e){
    //   this.triggerEvent('closecompent')
    // }

})