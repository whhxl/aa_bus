// pages/common/adress.js
var app = getApp()
Component({

  /**
   * 页面的初始数据
   */
  data: {
  adress: ['广州','深圳','珠海','澳门','香港']
  },
  methods:{
    choseadress(e){
      this.triggerEvent('mychoseadres', e)
    },
    closeadrescompent(e){
      this.triggerEvent('closecompent')
    }
  }

})