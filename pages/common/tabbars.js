const app = getApp()
Component({
  data:{
    list:[
      { icon: 'icon-clock', text: '预定', link: '../index/index', isactive: 'actived'},
      { icon: 'icon-card', text: '乘车', link: '../card/card', isactive: 'card' },
      { icon: 'icon-me', text: '我的', link: '../personal/personal', isactive: 'personal' },
    ],
    currentPage: ''
  },
  ready(){
    this.setcurrente()
  },
  methods:{
    setcurrente(){
      const currentUrl = [...getCurrentPages()].pop().route
      this.data.list.forEach(item=>{
        const currentlink = 'pages/' + item.link.split('../')[1]
        if(currentlink == currentUrl){
          this.setData({
            currentPage: item.isactive
          })
        }
      })
    }
  }
})