// pages/personal/chosepersonal.js
const app = getApp()
import { createSign } from "../../utils/util.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listLen: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },
  getList(){
    const session_id = app.globalData.session_id
    const apiurl = app.globalData.damain + "IdCard/index"
    const data = {
      session_id,
      ...createSign()
    }
    app.http(data, apiurl).then(res => {
      if (res) {
        res.data.list.forEach(item => {
          item.checked = 0
        })
        this.setData({
          list: res.data.list,
          chosenum: res.data.allow_choice_total
        })
      }
    })
  },
  handelChose(e){
    const indexs = e.currentTarget.dataset.index
    const list = this.data.list
    const limit = this.data.chosenum
    list.forEach((item,index)=>{
      if (index == indexs){
        item.checked = item.checked ? 0 : 1
        if (this.data.listLen == limit) {
          item.checked = 0
          wx.showToast({
            title: '乘客数量超出限制!',
            icon: 'none'
          })
        }
      }  
    })
    this.setData({
      list: list
    })
    this.setUser()
  },

  setUser(){
    var mylist = []
    const limit = this.data.chosenum
    const list = this.data.list
    list.forEach(item=>{
      if (item.checked == 1 && mylist.length < limit){
          mylist.push(item)
      }
    })
    this.setData({
      listLen: mylist.length,
      postList: mylist
    })
  },
  handelDele(e){
    const that = this
    const id = e.currentTarget.dataset.id
    const sessionid = app.globalData.session_id
    const apiurl = app.globalData.damain + 'IdCard/del'
    const data = {
      session_id: sessionid,
      cid: id,
      // ...createSign()
    }
    wx.showModal({
      title: '提示',
      content: '确定删除该乘客信息吗?',
      success(res) {
        if (res.confirm) {
          app.http(data, apiurl).then(resd => {
            if (resd.code == 1) {
              wx.showToast({
                title: resd.msg,
                icon: 'none'
              })
              that.getList()
            }
          })
        } else if (res.cancel) {
          wx.showToast({
            title: '已取消',
            icon: 'none'
          })
        }
      }
    })
   
  },
  handelEidit(e){
    const cid = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/personal/addpepole?type=1&id='+cid,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  addpersonal(e){
    wx.navigateTo({
      url: '/pages/personal/addpepole?type=1',
    })
  },
  backorder(e){
    const postuser = this.data.postList
    if (postuser.length==0){
      wx.showToast({
        title: '请添加乘客',
        icon: 'none'
      })
      return
    }
    wx.redirectTo({
      url: '/pages/order/confirm?postuser=' + JSON.stringify(postuser),
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})