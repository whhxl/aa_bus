// pages/order/orderdetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    descShow: false,
    visible: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },
  // 购票说明显示
  backticket() {
    this.setData({
      descShow: 1
    })
  },
  closepop() {
    this.setData({
      descShow: 0
    })
  },

  // 页面跳转
  tickets(){
    wx.navigateTo({
      url: '/pages/order/ticket',
    })
  },

  // 显示影藏弹框
  changeline(){
    this.setData({
      visible: true
    })
    // wx.navigateTo({
    //   url: '/pages/busline/changeline',
    // })
  },
  hidepop(){
    this.setData({
      visible: false
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})