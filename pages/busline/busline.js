// pages/busline/busline.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    teday: '',
    choseTime: '',
    showcalendar: 0,
    tabIndex:0,
    tabbars: ['所有班次','时间段筛选','上车点筛选','下车点筛选'],
    loading: true,
    prev: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   this.setData({
     choseTime: options.choseday,
     chosedate: wx.getStorageSync('choseDate').choseday,
     teday: util.formateTime()
   })
    this.getList()
  },
  gotoorder(e){
    const scheduleid = e.currentTarget.dataset.id;
    const buytype = e.currentTarget.dataset.type
    wx.navigateTo({
      url: `../order/order?id=${scheduleid}&type=${buytype}`,
    })
  },
  selecttabr(e){
    const indexs = e.currentTarget.dataset.index
    this.setData({
      tabIndex: indexs
    })
  },

// 获取班次列表
getList(){
  const params = {
    car_date: this.data.chosedate,
    start: wx.getStorageSync('startplace'),
    arrive: wx.getStorageSync('endplace'),
  }
  app.http(params, app.globalData.damain+'Lines/index').then(res=>{
    if(res.data){
      this.setData({
        list: res.data.list,
        loading: false,
        prev: res.data.prev
      })
    }
  })
},
// 跳转前一天后一天
  prevdate(){
    if (this.data.prev == 0) {
      wx.showToast({
        title: '班次时间已失效',
        icon: 'none'
      })
      return
    }
    const date = this.data.chosedate
    const yestady = this.getNextDate(date,-1)
    this.setData({
      choseTime: yestady.dateShow,
      chosedate: yestady.date,
      loading: false
    })
    this.getList()
  },
  nextdate(){
    const date = this.data.chosedate
    const yestady = this.getNextDate(date, 1)
    this.setData({
      choseTime: yestady.dateShow,
      chosedate: yestady.date,
      loading: false
    })
    this.getList()
  },
   getNextDate(date, day) {
    var dd = new Date(date);
    dd.setDate(dd.getDate() + day);
    var y = dd.getFullYear();
    var m = dd.getMonth() + 1 < 10 ? "0" + (dd.getMonth() + 1) : dd.getMonth() + 1;
    var d = dd.getDate() < 10 ? "0" + dd.getDate() : dd.getDate();
    return {date: y + "-" + m + "-" + d, dateShow: y + '年' + m+ '月' + d+'日'};
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})