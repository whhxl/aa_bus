// pages/order/order.js
const app= getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 班次详情
    // const data = util.createSign();
    const parmse = {
      schedule_id: options.id,
      buy_type: options.type,
      // ...data
    }
    app.http(parmse, app.globalData.damain+'Lines/detail').then(res=>{
      if(res){
        res.data.up_site.forEach(item=>{
          item.status = 0
        })
        res.data.down_site.forEach(item=>{
          item.status = 0
        })
        this.setData({
          uplist: res.data.up_site,
          downlist: res.data.down_site,
          buy_type: res.data.buy_type,
          schedule_id: res.data.schedule_id,
          loading: false
        })
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },
  choseupstation(e){
    const id = e.currentTarget.dataset.id
    this.data.uplist.forEach(item => {
      if (item.site_id == id){
        item.status = 1
      }else{
        item.status = 0
      }
    })
    this.setData({
      uplist: this.data.uplist
    })
  },
  chosedownstation(e){
    const id = e.currentTarget.dataset.id
    this.data.downlist.forEach( item => {
      if (item.site_id == id) {
        item.status = 1
      } else {
        item.status = 0
      }
    })
    this.setData({
      downlist: this.data.downlist
    })
  },
  goTopage(){
    const upSite = this.data.uplist.find(item=> item.status == 1)
    const downSite = this.data.downlist.find(item=> item.status == 1)
    const buy_type = this.data.buy_type
    const schedule_id = this.data.schedule_id
    let untl = ''
    switch(true){
      case upSite==undefined: untl='请选择上车点'; break;
      case downSite == undefined: untl = '请选择下车点'; break;
    }
    if (untl != ''){
        wx.showToast({
          title: untl,
          icon: 'none'
        })
        return 
    }
    wx.setStorageSync('sitedetail', { upsite: upSite, downsite: downSite, buyType: buy_type, schedule_id: schedule_id})
    wx.navigateTo({
      url: '../order/confirm',
    })
  },



  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})