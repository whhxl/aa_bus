// pages/personal/addpepole.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    card_unm:'',
    loading: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.id){
      const apiurl = app.globalData.damain + 'IdCard/detail'
      app.http(
        { 
        cid: options.id, 
        session_id: app.globalData.session_id,
        },
        apiurl
        ).then(res=>{
            if(res.code == 1){
              this.setData({
                name: res.data.real_name,
                card_unm: res.data.card_num,
                cid: res.data.id,
                loading: false
              })
            }
        })
    }
    this.setData({
      type: options.type
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },
  confirmClick(e){
    const formdata = e.detail.value;
    const apiurl = app.globalData.damain + 'IdCard/add'
    let intl = '';
   switch(true){
     case formdata.name == '' : intl ='请输入乘客姓名'; break;
     case formdata.card_unm == '': intl = '请输入乘客身份证号码'; break;
   }
   if(intl != ''){
     wx.showToast({
       title: intl,
       icon: 'none'
     })
     return
   }
   const data = {
      cid: '' || this.data.cid,
      session_id: app.globalData.session_id,
      name: formdata.name,
      card_num: formdata.card_unm,
      // ...createSign()
   }
    app.http(data,apiurl).then(res=>{
      if(res.code){
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
        if(this.data.type == 1){
          wx.redirectTo({
            url: '/pages/personal/chosepersonal',
          })
        }else if(this.data.type == 2){
          wx.redirectTo({
            url: '/pages/personal/pesonger',
          })
        }
      }else{
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      }
    })
  },
  formdata(e){
    return e.detail.value.replace('','')
  },
  // 取消事件
  restBack(){
    wx.navigateBack({
      delta: 1
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})