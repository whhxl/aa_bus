// pages/personal/pesonger.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
  list: [],
  loading: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },
  getList() {
    const apiurl = app.globalData.damain + "IdCard/index"
    const data = {
      session_id: app.globalData.session_id
    }
    app.http(data, apiurl).then(res => {
      if (res) {
        res.data.list.forEach(item => {
          item.checked = 0
        })
        this.setData({
          list: res.data.list,
          chosenum: res.data.allow_choice_total,
          loading: false
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  // 添加乘客
  toaddpeople(){
      wx.navigateTo({
        url: '/pages/personal/addpepole?type=2',
      })
  },
  handelDele(e) {
    const that = this
    const apiurl = app.globalData.damain + 'IdCard/del'
    const data = {
      session_id: app.globalData.session_id,
      cid: e.currentTarget.dataset.id,
    }
    wx.showModal({
      title: '提示',
      content: '确定删除该乘客信息吗?',
      success(res) {
        if (res.confirm) {
          that.setData({loading: true})
          app.http(data, apiurl).then(resd => {
            if (resd.code == 1) {
              wx.showToast({
                title: resd.msg,
                icon: 'none'
              })
              that.getList()
            }
          })
        } else if (res.cancel) {
          wx.showToast({
            title: '已取消',
            icon: 'none'
          })
        }
      }
    })

  },
  handelEidit(e) { 
    const cid = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/personal/addpepole?type=2&id=' + cid,
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})