// pages/order/confirm.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
      descShow: 0 ,
      loading: true,
      status: false,
      status1:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.postuser){
      this.setData({
        userlist: JSON.parse(options.postuser)
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
// 提交订单
  confirms(){
   wx.navigateTo({
     url: '../order/orderdetail',
   })
  },
  addpersger() {
    wx.navigateTo({
    url: '../personal/chosepersonal',
    })
  },
  backticket(){
    this.setData({
      descShow: 1
    })
  },
  closepop(){
    this.setData({
      descShow: 0
    })
  },
  // 移除乘客
  handelDels(e){
    const personlist = this.data.userlist
    const indexs = e.currentTarget.dataset.index
    wx.showModal({
      title: '提示',
      content: '确定移除该乘客吗?',
      success:(res)=>{
        if (res.confirm){
          personlist.splice(indexs,1)
          this.setData({
            userlist: personlist
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const sitemsg = wx.getStorageSync('sitedetail');
    const day = wx.getStorageSync('choseDate').choseday;
    this.setData({
      startday: day,
      upstite: sitemsg.upsite.site_name,
      uptime: sitemsg.upsite.site_time,
      downsite: sitemsg.downsite.site_name
    })
    this.getTickmsg()
  },
  // 获取订单页面信息
  getTickmsg(){
    const sitemsg = wx.getStorageSync('sitedetail');
    const apiurl=app.globalData.damain + 'Lines/buy';
    const data = {
        schedule_id : sitemsg.schedule_id,
        buy_type : sitemsg.buyType,
        up:sitemsg.upsite.site_name,
        down : sitemsg.downsite.site_name,
        session_id: app.globalData.session_id
      }
    app.http(data,apiurl).then(res=>{
      if(res.code == 1){
        this.setData({
          buydetail: res.data,
          loading: false,
        })
      }
    })
  },
  // 计算商品价格
  setprice(){

  },
  // 选择支付方式
  chosePayType(e){
    const indexs = e.currentTarget.dataset.type
    if(indexs == 2){
      this.setData({
        status: true,
        status1: false
      })
    }
    if(indexs == 1){
      this.setData({
        status: false,
        status1: true
      })
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})