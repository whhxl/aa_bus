// pages/common/calendar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    dataset: {
      type: String,
    },
    choseday:{
      type: String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    showdatePicker: false,
    weekLenght: 7,
    week: ['日', '一', '二', '三', '四', '五', '六'],
    dateList: [],
    weekNum: 0,
    tapthis: 0,
    thismouth: 0,
    thisyear: 0,
    dayIndex: 0,
    chooseDate: ""
  },
  ready(){
    this.getnowday()
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getnowday(){
      let datas = this.dataset.dataset
      let choseday = this.dataset.choseday
      let day = datas.split('-')[2],
        year = datas.split('-')[0],
        mouth = datas.split('-')[1]
      this.getweek(year, mouth, day)
      if (choseday != ''){
        this.setData({
          tapthis: choseday.split('-')[2]-1
        })
      }
    },
    getweek(year, month, day) {
      let that = this;
      let theDate = new Date();
      // console.log(D)
      theDate.setFullYear(year);
      theDate.setMonth(month - 1);
      theDate.setDate(1);

      let n = theDate.getDay(), arr = [], Index = 0, dayN = 1;
      for (let i = 0; i < day; i++) {
        arr.push(dayN++)
      }
      // console.log(arr)
      let now = new Date();
      let nowYear = now.getFullYear();
      let nowMonth = now.getMonth() + 1;
      let nowDay = now.getDate();
      let val = 1;
      if (year == nowYear) {
        if (month == nowMonth) {
          Index = arr.indexOf(nowDay);
          val = nowDay;
        }
      }
      that.setData({
        weekNum: n,
        dateList: arr,
        dayIndex: Index,
        tapthis: Index,
        thismonth: month,
        thisyear: year,
        chooseDate: year + "-" + month + "-" + val,
      })
    },
    chooseday(e) {
      var that = this;
      var n = e.currentTarget.dataset.index;
      var val = e.currentTarget.dataset.day;
      if (n >= that.data.dayIndex) {
        that.setData({
          tapthis: n,
          chooseDate: that.data.thisyear + "-" + that.data.thismonth + "-" + val,
          showModal: true,
        })
        this.triggerEvent('myevent', that.data.thisyear + "-" + that.data.thismonth + "-" + val)
      }else{
        wx.showToast({
          title: '你选择的日期已过期',
          icon: 'none'
        })
      } 
    },
    changemonth(e) {
      const that = this
      const types = e.currentTarget.dataset.type
      if (types == "prev") {
        let months = that.data.thismonth - 1
        let years = that.data.thisyear
        if (months < 1) {
          months = 12
          years = that.data.thisyear - 1
        }
        that.setData({
          thismonth: months,
          thisyear: years
        })
        that.getweek(years, months, that.getDaysInOneMonth(years, months))
      }
      if (types == "next") {
        let months = Math.floor(that.data.thismonth) + 1
        let years = that.data.thisyear
        if (months > 12) {
          months = 1
          years = Math.floor(that.data.thisyear) + 1
        }
        that.setData({
          thismonth: months,
          thisyear: years
        })
        that.getweek(years, months, that.getDaysInOneMonth(years, months))
      }
    },
    getDaysInOneMonth(year, month) {
      month = parseInt(month, 10);
      var d = new Date(year, month, 0)
      return d.getDate();
    },
    // 关闭组件
    gobacks(e) {
      this.triggerEvent('closecompent')
    }
  },
 
})
