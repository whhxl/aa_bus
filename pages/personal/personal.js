// pages/personal/personal.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_bind: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const userinfo = app.globalData.userInfo
    this.setData({
      userInfo: userinfo
    })
    // 检验是否绑定手机号
    const session_id = app.globalData.session_id
    app.http({ session_id }, app.globalData.damain + 'Users/check_bind').then(res => {
      if (res.code == 1 && res.data.is_bind == 1) {
        this.setData({
          is_bind: 1
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  gotoPage(){
    if(this.data.is_bind == 1){
      wx.navigateTo({
        url: '/pages/commphone/mytel',
      })
    }
    if(this.data.is_bind == 0){
      wx.navigateTo({
        url: '/pages/commphone/commphone',
      })
    }
  },
  gotopsongerPage() {
    wx.navigateTo({
      url: '/pages/personal/pesonger',
    })
  },
  gotomyorder(){
    wx.navigateTo({
      url: '/pages/order/myorder',
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})